package com.example.factoryapp.Activity;

import android.content.Intent;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;

import com.example.factoryapp.R;
import com.example.factoryapp.model.Role;
import com.example.factoryapp.model.User;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;


public class ConnectionActivity extends AppCompatActivity {


    private TextView result;
    private EditText userNameEdt, RomeId;
    private Button connect, create;
    private DatabaseReference DB, viewerDB;
    long viewerId = 0;
    Map<String, Object> map;


    User UserInfo;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_connection);


        userNameEdt = findViewById(R.id.UserName);
        RomeId = findViewById(R.id.RoomId);
        create = findViewById(R.id.create);
        UserInfo = new User();
        connect = findViewById(R.id.connection);
        result = findViewById(R.id.result);


        String roomID = RomeId.getText().toString();



        DB = FirebaseDatabase.getInstance().getReference().child("RoomDatabase");



//        viewerDB.addValueEventListener(new ValueEventListener() {
//            @Override
//            public void onDataChange(@NonNull DataSnapshot snapshot) {
//                if (snapshot.exists()){
//                    viewerId =(snapshot.getChildrenCount());
//                }
//            }
//
//            @Override
//            public void onCancelled(@NonNull DatabaseError error) {
//
//            }
//        });


        create.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(ConnectionActivity.this, CreateActivity.class));
            }
        });


        connect.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                int room = Integer.parseInt(RomeId.getText().toString());
                viewerDB = FirebaseDatabase.getInstance().getReference().child("RoomDatabase")
                        .child(String.valueOf(room))
                        .child("viewer");
                viewerDB.addValueEventListener(new ValueEventListener() {
                    @Override
                    public void onDataChange(@NonNull DataSnapshot snapshot) {
                        if (snapshot.exists()) {
                            viewerId = (snapshot.getChildrenCount());
                        }
                    }

                    @Override
                    public void onCancelled(@NonNull DatabaseError error) {

                    }
                });


                String name = userNameEdt.getText().toString();



                if (TextUtils.isEmpty(name) && TextUtils.isEmpty(RomeId.getText())) {

                    Toast.makeText(ConnectionActivity.this, "Please add some data.", Toast.LENGTH_SHORT).show();
                } else {
                    readData(name, room);
                    startActivity(new Intent(ConnectionActivity.this, ChoiceActivity.class));
                }
            }
        });
    }

    private void readData(String name, int room) {
        ArrayList<HashMap<String, Object>> upDated = new ArrayList<>();
        UserInfo.setRole(Role.User);
        UserInfo.setName(name);
        UserInfo.setReady(false);

//        DB.child(String.valueOf(room)).child("viewer").addListenerForSingleValueEvent(new ValueEventListener() {
//            @Override
//            public void onDataChange(@NonNull DataSnapshot snapshot) {
//                if (snapshot.exists()) {
//                    ArrayList<HashMap<String, Object>> map = (ArrayList<HashMap<String, Object>>) snapshot.getValue();
//                    upDated.addAll(map);
//                    System.out.println("Sonik :" + upDated);
//
////                    ArrayList<Object> mapViewer= (ArrayList<Object>) map.get("viewer");
//
//                    result.setText(String.valueOf(upDated));
//
//                }
//            }
//
//            @Override
//            public void onCancelled(@NonNull DatabaseError error) {
//
//            }
//        });

        viewerDB.child(String.valueOf(viewerId +1)).setValue(UserInfo);

    }
}