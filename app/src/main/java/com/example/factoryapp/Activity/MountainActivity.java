package com.example.factoryapp.Activity;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.widget.Button;
import android.widget.CheckBox;

import com.example.factoryapp.R;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import java.util.Map;

public class MountainActivity extends AppCompatActivity {
    private CheckBox checkBox1, checkBox2, checkBox3;
    private Button addToList;
    private DatabaseReference DB;
    long counter = 0;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_mountain);
        checkBox1 = findViewById(R.id.checkBox1);
        checkBox2 = findViewById(R.id.checkBox2);
        checkBox3 = findViewById(R.id.checkBox3);
        addToList = findViewById(R.id.addToList);

        DB = FirebaseDatabase.getInstance().getReference().child("Genres").child("Mountains").child("Choice");
        DB.child("1").addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot snapshot) {
                if (snapshot.exists()) {
                    Map<String,Object> map= (Map<String, Object>) snapshot.getValue();
                    Object name=map.get("mountainName");
                    System.out.println("Sonik : "+name);
                    checkBox1.setText((CharSequence) name);
                }
            }

            @Override
            public void onCancelled(@NonNull DatabaseError error) {

            }

        });
        DB.child("2").addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot snapshot) {
                if (snapshot.exists()) {
                    Map<String,Object> map= (Map<String, Object>) snapshot.getValue();
                    Object name=map.get("mountainName");
                    System.out.println("Sonik : "+name);
                    checkBox2.setText((CharSequence) name);
                }
            }

            @Override
            public void onCancelled(@NonNull DatabaseError error) {

            }

        });
        DB.child("3").addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot snapshot) {
                if (snapshot.exists()) {
                    Map<String,Object> map= (Map<String, Object>) snapshot.getValue();
                    Object name=map.get("mountainName");
                    System.out.println("Sonik : "+name);
                    checkBox3.setText((CharSequence) name);
                }
            }

            @Override
            public void onCancelled(@NonNull DatabaseError error) {

            }

        });

    }
}