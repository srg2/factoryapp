package com.example.factoryapp.Activity;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;

import com.example.factoryapp.R;

public class ChoiceActivity extends AppCompatActivity {


    private Button Mountain, City;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_choice2);

        Mountain = findViewById(R.id.Mountain);
        City = findViewById(R.id.City);

        Mountain.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(ChoiceActivity.this, MountainActivity.class));
            }
        });


        City.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(ChoiceActivity.this, CityActivity.class));
            }
        });


    }
}