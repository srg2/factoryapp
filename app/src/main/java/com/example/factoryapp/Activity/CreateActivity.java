package com.example.factoryapp.Activity;

import android.content.Intent;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;

import com.example.factoryapp.R;
import com.example.factoryapp.model.Role;
import com.example.factoryapp.model.Room;
import com.example.factoryapp.model.User;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import java.util.ArrayList;

public class CreateActivity extends AppCompatActivity {
    // creating variables for
    // EditText and buttons.

    private EditText UserNameEdt, capacityEdt;
    private TextView RoomId;
    private Button create;
    private DatabaseReference reff;
    long roomId =0;
    // creating a variable for
    // our object class
    User userInfo;
    Room roomInfo;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_create);

        // initializing our edittext and button
        UserNameEdt = findViewById(R.id.UserName);
        capacityEdt = findViewById(R.id.capacity);
        RoomId = findViewById(R.id.RoomId);
        reff=FirebaseDatabase.getInstance().getReference().child("RoomDatabase");
        reff.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot snapshot) {
                if (snapshot.exists()){
                    roomId =(snapshot.getChildrenCount());
                }
            }

            @Override
            public void onCancelled(@NonNull DatabaseError error) {

            }
        });


        // initializing our object
        // class variable.
        userInfo = new User();
        roomInfo = new Room();

        create = findViewById(R.id.create);


        // adding on click listener for our button.
        create.setOnClickListener(new View.OnClickListener() {




            @Override
            public void onClick(View v) {

                // getting text from our edittext fields.

                String name = UserNameEdt.getText().toString();
                String capacity = capacityEdt.getText().toString();


                System.out.println("sonik: User: " +name+" "+capacity);

                // below line is for checking weather the
                // edittext fields are empty or not.
                if (TextUtils.isEmpty(name) && TextUtils.isEmpty(capacity)) {
                    System.out.println("sonik: If: False");
                    startActivity(new Intent(CreateActivity.this, ConnectionActivity.class));
                    // if the text fields are empty
                    // then show the below message.
                    Toast.makeText(CreateActivity.this, "Please add some data.", Toast.LENGTH_SHORT).show();
                } else {
                    System.out.println("sonik: If: True");
                    // else call the method to add
                    // data to our database.
                    addDatatoFirebase(name, capacity);

                    startActivity(new Intent(CreateActivity.this, ChoiceActivity.class));
                }
            }
        });
    }

    private void addDatatoFirebase(String name, String chcapacity) {
        // below 3 lines of code is used to set
        // data in our object class.

        int userId = 1;
        int capacity = Integer.parseInt(chcapacity);
        ArrayList<User> list= new ArrayList<User>(capacity);



        userInfo.setId(userId);
        userInfo.setName(name);
        userInfo.setRole(Role.Admin);
        userInfo.setReady(false);

        System.out.println("sonik: User:"+userInfo );
        list.add(userInfo);

        roomInfo.setCapacity(capacity);
        roomInfo.setViewer(list);

        System.out.println("sonik: Room:"+ roomInfo);

        String ans= String.valueOf(roomId+1);

        RoomId.setText(ans);

        System.out.println("sonik: User:"+ roomId);


        addData(userInfo,roomInfo);
    }


    private void addData(User userInfo, Room RoomInfo)
    {



//        FirebaseDatabase.getInstance().getReference().child("UsersDatabases").push()
//                .setValue(userInfo)
//                .addOnSuccessListener(new OnSuccessListener<Void>() {
//                    @Override
//                    public void onSuccess(Void aVoid) {
//                        Toast.makeText(getApplicationContext(),"Inserted Successfully",Toast.LENGTH_LONG).show();
//                    }
//                })
//                .addOnFailureListener(new OnFailureListener() {
//                    @Override
//                    public void onFailure(@NonNull Exception e)
//                    {
//                        Toast.makeText(getApplicationContext(),"Could not insert",Toast.LENGTH_LONG).show();
//                    }
//                });



//        FirebaseDatabase.getInstance().getReference().child("RoomsDatabases").push()
//                .setValue(RoomInfo)
//                .addOnSuccessListener(new OnSuccessListener<Void>() {
//                    @Override
//                    public void onSuccess(Void aVoid) {
//                        Toast.makeText(getApplicationContext(),"Inserted Successfully",Toast.LENGTH_LONG).show();
//                    }
//                })
//                .addOnFailureListener(new OnFailureListener() {
//                    @Override
//                    public void onFailure(@NonNull Exception e)
//                    {
//                        Toast.makeText(getApplicationContext(),"Could not insert",Toast.LENGTH_LONG).show();
//                    }
//                });



                reff.child(String.valueOf(roomId +1)).setValue(RoomInfo);
                // after adding this data we are showing toast message.
                Toast.makeText(CreateActivity.this, "data added", Toast.LENGTH_SHORT).show();

    }

}