package com.example.factoryapp.DAO;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.example.factoryapp.R;
import com.example.factoryapp.model.Room;

import java.util.List;

public class RecyclerView_Config {
    private Context myContext;
    private RoomAdapter myRoomAdapter;


    public void setConfig(RecyclerView recyclerView,Context context,List<Room> rooms,List<String>keys){
        myContext=context;
        myRoomAdapter =new RoomAdapter(rooms,keys);
        recyclerView.setLayoutManager(new LinearLayoutManager(context));
        recyclerView.setAdapter(myRoomAdapter);
    }


    class RoomItemView extends RecyclerView.ViewHolder {
        private TextView name,ready,role;
        private String key;

        public RoomItemView( ViewGroup parent) {
            super(LayoutInflater.from(myContext).
                    inflate(R.layout.wait_list,parent,false));


        name=itemView.findViewById(R.id.name);
        ready=itemView.findViewById(R.id.ready);
        role=itemView.findViewById(R.id.wrole);
        }

        public void bind(Room room,String key){
            name.setText(room.getCapacity());
            role.setText(room.getCapacity());
            ready.setText((CharSequence) room.getViewer());
            this.key=key;
        }
    }
    class RoomAdapter extends RecyclerView.Adapter<RoomItemView>{
        private List<Room> myRoomList;
        private List<String> myKey;

        public RoomAdapter(List<Room> myRoomList, List<String> myKey) {
            this.myRoomList = myRoomList;
            this.myKey = myKey;
        }


        @Override
        public RoomItemView onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
            return new RoomItemView(parent);
        }

        @Override
        public void onBindViewHolder(@NonNull RoomItemView holder, int position) {
            holder.bind(myRoomList.get(position),myKey.get(position));
        }

        @Override
        public int getItemCount() {
            return myRoomList.size();
        }
    }
}
