package com.example.factoryapp.DAO;

import androidx.annotation.NonNull;

import com.example.factoryapp.model.Room;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import java.util.ArrayList;
import java.util.List;

public class FirebaseDataHelper {
    private FirebaseDatabase myDatabase;
    private DatabaseReference myReference;
    private List<Room> rooms = new ArrayList<>();


    public interface DataStatus{
        void DataIsLoaded(List<Room> rooms, List<String> keys);
        void DataIsInserted();
        void DataIsUpdated();
        void DataIsDeleted();
    }



    public FirebaseDataHelper(){
        myDatabase = FirebaseDatabase.getInstance();
        myReference=myDatabase.getReference("RoomDatabase");
    }

    public void readRooms(final DataStatus dataStatus){
        myReference.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot snapshot) {

                rooms.clear();
                List<String> keys = new ArrayList<>();
                for (DataSnapshot keyNode: snapshot.getChildren()){
                     keys.add(keyNode.getKey());
                     Room room =keyNode.getValue(Room.class);
                    rooms.add(room);
                }
                dataStatus.DataIsLoaded(rooms,keys);

            }

            @Override
            public void onCancelled(@NonNull DatabaseError error) {

            }
        });
    }

}
