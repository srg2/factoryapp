package com.example.factoryapp.model;

public class User {
    private int Id;
    private String name;
    private Role role;
    private Boolean ready;
//    private int RoomId;



    public User() {
    }

    public User(int id, String name, Role role, Boolean ready) {
        Id = id;
        this.name = name;
        this.role = role;
        this.ready = ready;
    }

    public int getId() {
        return Id;
    }

    public void setId(int id) {
        Id = id;
    }

    public Role getRole() {
        return role;
    }

    public void setRole(Role role) {
        this.role = role;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Boolean getReady() {
        return ready;
    }

    public void setReady(Boolean ready) {
        this.ready = ready;
    }
}
