package com.example.factoryapp.model;

import java.util.ArrayList;

public class Room {
    private int Id;
    private int Capacity;
    private ArrayList<User> Viewer;
    private ArrayList<String> Places;



    public Room() {
    }

    public Room(int id, int capacity, ArrayList<User> viewer, ArrayList<String> places) {
        Id = id;
        Capacity = capacity;
        Viewer = viewer;
        Places = places;
    }

    public int getId() {
        return Id;
    }

    public void setId(int id) {
        Id = id;
    }

    public ArrayList<User> getViewer() {
        return Viewer;
    }

    public void setViewer(ArrayList<User> viewer) {
        Viewer = viewer;
    }

    public int getCapacity() {
        return Capacity;
    }

    public void setCapacity(int capacity) {
        Capacity = capacity;
    }

    public ArrayList<String> getPlaces() {
        return Places;
    }

    public void setPlaces(ArrayList<String> places) {
        Places = places;
    }
}
